INSERT INTO empleado (nombre, apellido_materno, apellido_paterno, fecha_ingreso) VALUES ('David','Ayala','Juarez','2015-01-02');
INSERT INTO empleado (nombre, apellido_materno, apellido_paterno, fecha_ingreso) VALUES ('Andres','Guzman','Ramirez','2016-01-02');
INSERT INTO empleado (nombre, apellido_materno, apellido_paterno, fecha_ingreso) VALUES ('Richard','Doe','Smith','2010-01-02');
INSERT INTO empleado (nombre, apellido_materno, apellido_paterno, fecha_ingreso) VALUES ('Yessid','Lozano','Alvares','2009-01-02');
INSERT INTO empleado (nombre, apellido_materno, apellido_paterno, fecha_ingreso) VALUES ('Jose Luis','Aguilar','Soto','2018-01-02');
INSERT INTO empleado (nombre, apellido_materno, apellido_paterno, fecha_ingreso) VALUES ('Janette','Morales','Garcia','2018-01-02');
INSERT INTO empleado (nombre, apellido_materno, apellido_paterno, fecha_ingreso) VALUES ('Jessica','Mota','Sanchez','2011-01-02');
INSERT INTO empleado (nombre, apellido_materno, apellido_paterno, fecha_ingreso) VALUES ('Angel','Morales','Garcia','2018-01-02');


INSERT INTO vacaciones(id_registro, fecha,dia_tomados, id_Empleado) VALUES(1, '2018-05-18', 1, 1);
INSERT INTO vacaciones(id_registro, fecha,dia_tomados, id_Empleado) VALUES(2, '2018-05-19', 1, 1);
INSERT INTO vacaciones(id_registro, fecha,dia_tomados, id_Empleado) VALUES(3, '2018-02-20', 1, 2);
INSERT INTO vacaciones(id_registro, fecha,dia_tomados, id_Empleado) VALUES(4, '2018-06-08', 1, 3);
INSERT INTO vacaciones(id_registro, fecha,dia_tomados, id_Empleado) VALUES(5, '2018-12-22', 1, 4);
INSERT INTO vacaciones(id_registro, fecha,dia_tomados, id_Empleado) VALUES(6, '2016-02-02', 1, 1);

INSERT INTO role (id, name) VALUES(1, 'ADMIN');
INSERT INTO role (id, name) VALUES(2, 'USER');

INSERT INTO user (id, password, username) VALUES(1, '$2a$10$oA.74RwpuifvaWhkZHS9r.Kc4coVeMVAyRfCo/VceydzCn94Irrp2', 'user');

INSERT INTO user_role (user_id, role_id) VALUES(1,1); 


