package com.beeva.people.vacaciones.services;

import com.beeva.people.vacaciones.empleado.pojo.Role;
import com.beeva.people.vacaciones.empleado.pojo.User;
import com.beeva.people.vacaciones.empleado.repository.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Set;

@Service
public class UserDetailServiceImpl implements UserDetailsService{
    @Autowired
    private UserRepository userRepository;

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username);

        Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
        for (Role role : user.getRoles()){
            grantedAuthorities.add(new SimpleGrantedAuthority(role.getName()));
        }

//        System.out.println("ESTE USUARIO SOLO SE LE REGISTRAN " + grantedAuthorities.size());
//        System.out.println("*************************");
//        grantedAuthorities.stream().map(a -> {return a.getAuthority();}).forEach(System.out::println);
//        System.out.println("*************************");
//


        UserDetails userDetails =  new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), grantedAuthorities);



        return userDetails;
    }
}
