package com.beeva.people.vacaciones.services;


import com.beeva.people.vacaciones.empleado.pojo.User;

public interface UserService {
    void save(User user);

    User findByUsername(String username);
}
