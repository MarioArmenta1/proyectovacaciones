package com.beeva.people.vacaciones.services;

import com.beeva.people.vacaciones.empleado.pojo.Role;
import org.springframework.stereotype.Service;

@Service
public interface RoleService {

    Role findByName(String name);
}
