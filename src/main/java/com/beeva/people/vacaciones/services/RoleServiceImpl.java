package com.beeva.people.vacaciones.services;

import com.beeva.people.vacaciones.empleado.pojo.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.stereotype.Service;

@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    private JdbcOperations operations;

    @Override
    public Role findByName(String name) {
        String sql = "SELECT * FROM role WHERE name = ?";
        Role role = (Role) operations.queryForObject(sql,new Object[] { name }, new BeanPropertyRowMapper(Role.class));
        return role;
    }
}
