package com.beeva.people.vacaciones.empleado.service;

import java.util.Date;
import java.util.List;

import com.beeva.people.vacaciones.empleado.pojo.RegistroVacaciones;

public interface RegistroVacacionesService {

	public List<RegistroVacaciones> findAll();

	public RegistroVacaciones save(RegistroVacaciones registro);

	public List<RegistroVacaciones> findByIdEmpleado(Long idEmpleado);

	public List<RegistroVacaciones> findByIdEmpleadoAndYear(Long idEmpleado, Integer anio);

	public Long countByIdEmpleado(Long idEmpleado);

	public Date saveByFecha(Date date, Long id_registro);

}
