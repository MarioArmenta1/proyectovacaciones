package com.beeva.people.vacaciones.empleado.service;

import java.sql.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.beeva.people.vacaciones.empleado.pojo.Empleado;
import com.beeva.people.vacaciones.empleado.repository.EmpleadoRepository;

@Service
public class EmpleadoServiceImp implements EmpleadoService {

	@Autowired
	private EmpleadoRepository empleadoRepository;

	@Override
	public List<Empleado> findAll() {
		return (List<Empleado>) empleadoRepository.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Empleado findById(Long id) {
		return empleadoRepository.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public Empleado save(Empleado empleado) {
		return empleadoRepository.save(empleado);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		empleadoRepository.deleteById(id);
	}

	@Override
	@Transactional(readOnly = true)
	public Date findDate(Long id) {
		return empleadoRepository.getDateCreate(id);
	}

	@Override
	public List<Empleado> findByNombre(String nombre) {
		return (List<Empleado>) empleadoRepository.findByNombre(nombre);
	}

	@Override
	public List<Empleado> findByNombreAndApellidoPaternoAndApellidoMaterno(String nombre, String apellidoPat,
			String apellidoMat) {
		return empleadoRepository.findByNombreAndApellidoPaternoAndApellidoMaterno(nombre, apellidoPat, apellidoMat);
	}

	@Override
	public List<Empleado> findEmpeladoById(Long id) {
		return empleadoRepository.findEmpeladoById(id);
	}

	public Iterable<Empleado> findEmpleados(Empleado empleado) {
		return empleadoRepository.findAll(Example.of(empleado));
	}

}
