package com.beeva.people.vacaciones.empleado.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/import")
public class archivoController {
	@RequestMapping("/excelBD")
	public ResponseEntity<String> uploadFile(@RequestParam("uploadedFile") MultipartFile uploadedFileRef) {
		System.out.println("Entramos al controlador");

		// Se consigue el nombre del archivo
		String fileName = uploadedFileRef.getOriginalFilename();
		System.out.println("El archivo es este: " + fileName + " y se guardará en " + System.getProperty("user.dir")
				+ "/src/main/resources/");

		// path donde el archivo de va a almacenar
		String path = System.getProperty("user.dir") + "/src/main/resources/" + fileName;

		// Buffer que almacenara los datos leidos
		byte[] buffer = new byte[1024];

		// creamos el archivo en el server
		File outputFile = new File(path);

		FileInputStream reader = null;
		FileOutputStream writer = null;

		int totalBytes = 0;

		try {
			outputFile.createNewFile();

			// Se crea el lector para el archivo que se envó
			reader = (FileInputStream) uploadedFileRef.getInputStream();

			// Se crea el escritor para el archivo nuevo e el server
			writer = new FileOutputStream(outputFile);

			// se lee y escribe el el archivo obtenido en el nuevo en el server
			int bytesRead = 0;
			while ((bytesRead = reader.read(buffer)) != -1) {
				System.out.println("bytes leidos en iteracion: " + bytesRead);
				writer.write(buffer, 0, bytesRead);
				totalBytes += bytesRead;
			}
		} catch (IOException e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		} finally {
			try {
				reader.close();
				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		HttpHeaders headers = new HttpHeaders();
		headers.add("Status", "Archivo subido correctamente");
		return ResponseEntity.ok().headers(headers).body("Ok");
	}

	@RequestMapping("/download")
	public ResponseEntity<Resource> download(@RequestParam("file") String archivo) throws IOException {

		File file = new File(System.getProperty("user.dir") + "/src/main/resources/" + archivo);

		InputStreamResource resourse = new InputStreamResource(new FileInputStream(file));

		HttpHeaders headers = new HttpHeaders();
		// headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
		// headers.add("Pragma", "no-cache");
		// headers.add("Expires", "0");
		headers.add("Content-Disposition", "attachment;filename=" + file.getName());

		return ResponseEntity.ok().headers(headers).contentType(MediaType.parseMediaType("application/octet-stream"))
				.contentLength(file.length()).body(resourse);
	}
}
