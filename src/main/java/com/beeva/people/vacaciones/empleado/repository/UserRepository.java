package com.beeva.people.vacaciones.empleado.repository;

import com.beeva.people.vacaciones.empleado.pojo.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

public interface UserRepository extends JpaRepository<User, Integer> {

    User findByUsername(String username);

}
