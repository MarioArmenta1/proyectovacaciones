package com.beeva.people.vacaciones.empleado.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import com.beeva.people.vacaciones.dto.EmpleadoBasicoDTO;
import com.beeva.people.vacaciones.dto.EmpleadoRespuestaDTO;
import com.beeva.people.vacaciones.dto.RegistroRespuestaDTO;
import com.beeva.people.vacaciones.dto.VacacionesDTO;
import com.beeva.people.vacaciones.empleado.pojo.Empleado;
import com.beeva.people.vacaciones.empleado.pojo.RegistroVacaciones;
import com.beeva.people.vacaciones.empleado.service.EmpleadoService;
import com.beeva.people.vacaciones.empleado.service.RegistroVacacionesService;
import com.beeva.people.vacaciones.utils.RegistrarVacaciones;
import com.beeva.people.vacaciones.utils.Registro;

@RestController
@RequestMapping("/control-empleados")
public class EmpleadoRestController {

	@Autowired
	private EmpleadoService empleadoService;

	@Autowired
	private RegistroVacacionesService registroVacacionesService;

	@GetMapping("/consultar-empleados")
	@PreAuthorize("hasAuthority('ADMIN')")
	public List<Empleado> getAllEmpleados() {
		return empleadoService.findAll();
	}

	@GetMapping("/empleados/nombre/{nombre}")
	@PreAuthorize("hasAuthority('ADMIN')")
	public List<Empleado> getEmpleadoByNombre(@PathVariable String nombre) {
		return empleadoService.findByNombre(nombre);
	}

	@GetMapping("/dias-disponibles-empleado/{id}")
	@PreAuthorize("hasAuthority('ADMIN')")
	public int diasDisponiblesEmpleado(@PathVariable Long id) {
		int diasTrabajados = 0, diasPermitidos = 0;
		Empleado empleado = new Empleado();
		Date date = new Date();

		RegistrarVacaciones dias = new RegistrarVacaciones();

		empleado = empleadoService.findById(id);
		Date fechaInicial = empleado.getFechaIngreso();
		diasTrabajados = dias.DiasTranscurridos(fechaInicial, date);
		diasPermitidos = dias.DiasPermitidos(diasTrabajados);

		return diasPermitidos;
	}

	@GetMapping("/informacion-empleado/{id}")
	@PreAuthorize("hasAuthority('ADMIN')")
	public RegistroRespuestaDTO getRegistroByNombre(@PathVariable Long id) {
		Empleado datos = empleadoService.findById(id);
		Registro registro = new Registro();
		List<RegistroVacaciones> listaVacacionesFiltrada = new ArrayList<>();
		listaVacacionesFiltrada= registroVacacionesService.findByIdEmpleadoAndYear(datos.getNumeroEmpleado(),Calendar.getInstance().get(Calendar.YEAR));
		return registro.respuestaDTO(datos, listaVacacionesFiltrada);
	}

	@PostMapping("/registrar-fechas")
	@ResponseStatus(HttpStatus.CREATED)
	@PreAuthorize("hasAuthority('ADMIN')")
	public EmpleadoRespuestaDTO arrayVacaciones(@RequestBody VacacionesDTO vacacionesDTO) {
   		RegistrarVacaciones registrarVacaciones= new RegistrarVacaciones(); 
		List<RegistroVacaciones> listaRegistroVacaciones= new ArrayList<>();
		ArrayList<Date> listaDate = new ArrayList<>();
		EmpleadoRespuestaDTO dto = new EmpleadoRespuestaDTO();
		Registro registro = new Registro();
		listaRegistroVacaciones=registrarVacaciones.RegistroArrayVacaciones(vacacionesDTO);
		int diasPermitidos=0,diasTomados=0;
		Empleado empleado= new Empleado();
		
		empleado=empleadoService.findById(vacacionesDTO.getIdEmpleado());
		
		for(RegistroVacaciones registroVacaciones : listaRegistroVacaciones) {	
			diasPermitidos=registrarVacaciones.DiasPermitidos(registrarVacaciones.DiasTranscurridos(empleado.getFechaIngreso(),  registroVacaciones.getFecha()));
			diasTomados=registroVacacionesService.findByIdEmpleado(empleado.getNumeroEmpleado()).size();	
			if(diasPermitidos > diasTomados ) {
				listaDate.add(registroVacaciones.getFecha());
				registroVacacionesService.save(registroVacaciones);
				dto = registro.messageEmpleado(listaDate, empleado);
			}else if(diasPermitidos == 0){
				dto = registro.messageEmpleado(null, empleado);
			}
		}
		return dto;
	}

	@PostMapping("/buscar-empleados")
	@PreAuthorize("hasAuthority('ADMIN')")
	public List<Empleado> getEmpleadoByNombreAndApellidos(@RequestBody Empleado empleado) {
		return empleadoService.findByNombreAndApellidoPaternoAndApellidoMaterno(empleado.getNombre(),
				empleado.getApellidoPaterno(), empleado.getApellidoMaterno());
	}

	@PostMapping("/crear-empleado")
	@ResponseStatus(HttpStatus.CREATED)
	@PreAuthorize("hasAuthority('ADMIN')")
	public Empleado createEmpleado(@RequestBody Empleado empleado) {
		return empleadoService.save(empleado);
	}


	@PutMapping("/modificar-empleado/{id}")
	@ResponseStatus(HttpStatus.CREATED)
	@PreAuthorize("hasAuthority('ADMIN')")
	public Empleado updateEmpleado(@RequestBody Empleado empleado, @PathVariable Long id) {
		Empleado empleadoActual = empleadoService.findById(id);

		empleadoActual.setNombre(empleado.getNombre());
		empleadoActual.setApellidoMaterno(empleado.getApellidoMaterno());
		empleadoActual.setApellidoPaterno(empleado.getApellidoPaterno());

		return empleadoService.save(empleadoActual);
	}

	@GetMapping("/informacion-basica-empleadoDTO")
	@PreAuthorize("hasAuthority('ADMIN')")
	public List<EmpleadoBasicoDTO> getRegistroByNombre() {
		
		List<Empleado> empleadoList= new ArrayList<>();
		List<RegistroVacaciones> listaVacacionesFiltrada = new ArrayList<>();
		Registro registro = new Registro();
		List<EmpleadoBasicoDTO> listEmpleadoBasicoDTO = new ArrayList<>();

		empleadoList= empleadoService.findAll();
		for(Empleado empleado : empleadoList) {
		int diasTomados =0;
			listaVacacionesFiltrada= registroVacacionesService.findByIdEmpleadoAndYear(empleado.getNumeroEmpleado(),Calendar.getInstance().get(Calendar.YEAR));
			diasTomados=listaVacacionesFiltrada.size();
			listEmpleadoBasicoDTO.add(registro.empleadoBasicoDTO(empleado, diasTomados));
		}
		return listEmpleadoBasicoDTO;
	}

}
