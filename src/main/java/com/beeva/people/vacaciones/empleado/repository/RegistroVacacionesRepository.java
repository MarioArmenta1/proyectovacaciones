package com.beeva.people.vacaciones.empleado.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.beeva.people.vacaciones.empleado.pojo.RegistroVacaciones;

public interface RegistroVacacionesRepository extends JpaRepository<RegistroVacaciones, Long> {

	public List<RegistroVacaciones> findByIdEmpleado(Long idEmpleado);

	@Query("FROM RegistroVacaciones v WHERE v.idEmpleado = :id AND YEAR(v.fecha) = :anio")
	public List<RegistroVacaciones> findByIdEmpleadoAndYear(@Param("id") Long idEmpleado, @Param("anio") Integer anio);

	public Long countByIdEmpleado(Long idEmpleado);

	@Modifying
	@Query("update RegistroVacaciones u set u.fecha = :fecha where u.idRegistro = :idRegistro")
	Date setFechaByIdRegistro(@Param("fecha") Date fecha, @Param("idRegistro") Long idRegistro);

	// public Date saveByFecha(Date fecha);

}
