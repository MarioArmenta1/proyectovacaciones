package com.beeva.people.vacaciones.empleado.controller;


import com.beeva.people.vacaciones.empleado.pojo.User;
import com.beeva.people.vacaciones.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/registration")
public class Registration {

    @Autowired
    UserService userService;

    @PostMapping
    public void registration(@RequestBody User user) {
        System.out.println(user);

        userService.save(user);

    }
}
