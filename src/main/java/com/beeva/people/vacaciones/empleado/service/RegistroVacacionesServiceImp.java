package com.beeva.people.vacaciones.empleado.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.beeva.people.vacaciones.empleado.pojo.RegistroVacaciones;
import com.beeva.people.vacaciones.empleado.repository.RegistroVacacionesRepository;

@Service("springJpaRegistroVacacionesService")
@Transactional
public class RegistroVacacionesServiceImp implements RegistroVacacionesService {

	@Autowired
	private RegistroVacacionesRepository registroVacacionesRepository;

	@Transactional(readOnly = true)
	public List<RegistroVacaciones> findAll() {
		return registroVacacionesRepository.findAll();
	}

	@Override
	@Transactional
	public RegistroVacaciones save(RegistroVacaciones registroVacaciones) {
		return registroVacacionesRepository.save(registroVacaciones);
	}

	@Override
	public List<RegistroVacaciones> findByIdEmpleado(Long idEmpleado) {
		return registroVacacionesRepository.findByIdEmpleado(idEmpleado);
	}

	@Override
	public List<RegistroVacaciones> findByIdEmpleadoAndYear(Long idEmpleado, Integer anio) {
		return registroVacacionesRepository.findByIdEmpleadoAndYear(idEmpleado, anio);
	}

	@Override
	public Long countByIdEmpleado(Long idEmpleado) {
		return registroVacacionesRepository.countByIdEmpleado(idEmpleado);
	}

	@Override
	@Transactional(readOnly = true)
	public Date saveByFecha(Date fecha, Long id_registro) {
		return registroVacacionesRepository.setFechaByIdRegistro(fecha, id_registro);
	}

}
