package com.beeva.people.vacaciones.empleado.repository;

import com.beeva.people.vacaciones.empleado.pojo.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long>{


}
