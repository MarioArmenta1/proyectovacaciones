package com.beeva.people.vacaciones.config;


import com.beeva.people.vacaciones.services.UserDetailServiceImpl;
import io.jsonwebtoken.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;


import java.util.Date;


/**
 * This service class is which is responsible for all about JWT Token.
 *
 * @author juan
 */
public class TokenHandler {

    static final long EXPIRATIONTIME = 864_000_000; // 10 days
    static final String SECRET = "ThisIsASecret";
    static final String TOKEN_PREFIX = "Bearer ";
    static final String HEADER_STRING = "Authorization";

    private static final Logger LOGGER = LoggerFactory.getLogger(TokenHandler.class);

    @Autowired
    static UserDetailsService myUserDetails;
    @Autowired
    static UserDetailServiceImpl uds;

    /**
     * Method which add Authorization header to response.
     *
     * @param username username from basic authentication
     */

    static String getToken(String username) {
        return builJWTToken(username);
    }

    /**
     * Build JWT TOKEN
     * @param username
     * @return
     */

    static String builJWTToken(String username){
        String JWT = Jwts.builder()
                .setSubject(username)
                .setExpiration(new Date(System.currentTimeMillis() + EXPIRATIONTIME))
                .signWith(SignatureAlgorithm.HS512, SECRET)
                .compact();

        return TOKEN_PREFIX +" "+ JWT;
    }


    /**
     * This method verifies JWT Tokens veracity
     * and return sensitive info from JWT TOKEN,
     * if not, it return null and unchains 403 HTTP error
     *
     * @param token request from client to extract the jwt token and check it
     * @return instance of Authentication
     * @throws MalformedJwtException
     */
    static String decodeAndExtract(String token) {
        if (token != null && token.startsWith(TOKEN_PREFIX)) {
            try {
                return Jwts.parser()
                        .setSigningKey(SECRET)
                        .parseClaimsJws(token.replace(TOKEN_PREFIX, ""))
                        .getBody()
                        .getSubject();

            } catch (ExpiredJwtException e) {
                LOGGER.error("Error Report: " + e.getMessage());
                return null;
            } catch (UnsupportedJwtException e) {
                LOGGER.error("Error Report :" + e.getMessage());
                return null;
            } catch (MalformedJwtException e) {
                LOGGER.error("Error Report: " + e.getMessage());
                return null;
            } catch (SignatureException e) {
                LOGGER.error("Error Report: " + e.getMessage());
                return null;
            } catch (IllegalArgumentException e) {
                LOGGER.error("Error Report in URL :" + e.getMessage());
                return null;
            }
        } else {
            return null;
        }
    }
}
