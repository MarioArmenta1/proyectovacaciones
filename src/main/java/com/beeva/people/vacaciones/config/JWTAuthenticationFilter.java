package com.beeva.people.vacaciones.config;


import com.beeva.people.vacaciones.services.UserDetailServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Custom filter to get authentication
 *
 * @author juan
 */
public class JWTAuthenticationFilter extends OncePerRequestFilter {

    @Autowired
    private UserDetailServiceImpl userDetailsService;


    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
        if (httpServletRequest.getHeader("Authorization") != null) {

            UserDetails userDetails = userDetailsService.loadUserByUsername(TokenHandler.decodeAndExtract(httpServletRequest.getHeader(TokenHandler.HEADER_STRING)));

            UsernamePasswordAuthenticationToken authentication
                                                = new UsernamePasswordAuthenticationToken(
                                                        userDetails.getUsername(),
                                                null,
                                                        userDetails.getAuthorities());

            SecurityContextHolder.getContext().setAuthentication(authentication);
        } else {
            SecurityContextHolder.getContext().setAuthentication(null);
        }
        filterChain.doFilter(httpServletRequest, httpServletResponse);


    }
}