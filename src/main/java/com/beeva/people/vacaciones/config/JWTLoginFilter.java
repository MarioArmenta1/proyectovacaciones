package com.beeva.people.vacaciones.config;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Base64;


/**
 * This filter is called when the url indicated in the constructor is requested.
 */
public class JWTLoginFilter extends AbstractAuthenticationProcessingFilter {


    UserDetailsService myUserDetails;
    public JWTLoginFilter(String url, AuthenticationManager authManager, UserDetailsService myUserDetails) {
        super(new AntPathRequestMatcher(url));
        setAuthenticationManager(authManager);
        this.myUserDetails = myUserDetails;
    }

    /**
     * Method to perform an authentication process across the http basic auth.
     *
     * @param req request from client to server inside it there are user and password
     * @param res response from server to client
     * @return return an authentication.
     * @throws AuthenticationException
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public Authentication attemptAuthentication(HttpServletRequest req, HttpServletResponse res) throws AuthenticationException, IOException, ServletException {
        try {
            final String authorization = req.getHeader("Authorization");
            if (authorization != null && authorization.startsWith("Basic")) {

                String credentials = new String(Base64.getDecoder().decode(authorization.substring("Basic".length()).trim()),
                        Charset.forName("UTF-8"));

                return getAuthenticationManager().authenticate(

                        new UsernamePasswordAuthenticationToken(
                                credentials.split(":")[0],
                                credentials.split(":")[1],
                                null)

                );
            } else {
                return null;
            }

        }catch (Exception e ){
            e.printStackTrace();
            System.out.println("ERRPR" + e.getMessage());
            return null;
        }
    }

    /**
     * My default custom behavior on successful login, in this case this calls
     * to {@link TokenHandler} to built a valid token.
     *
     * @param req
     * @param res   necessary param which, through this the token will sent back to the client.
     * @param chain
     * @param auth
     * @throws IOException
     * @throws ServletException
     */
    @Override
    protected void successfulAuthentication(HttpServletRequest req, HttpServletResponse res, FilterChain chain, Authentication auth) throws IOException, ServletException {

        String token = TokenHandler.getToken(auth.getName());
        res.addHeader(TokenHandler.HEADER_STRING, token);

    }
}
