package com.beeva.people.vacaciones.dto;

import java.io.Serializable;
import java.util.ArrayList;

public class EmpleadoRespuestaDTO implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private ArrayList<String> fechas;
	private String nombre;
	private String mensaje;
	public ArrayList<String> getFechas() {
		return fechas;
	}
	public void setFechas(ArrayList<String> fechas) {
		this.fechas = fechas;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	
}
