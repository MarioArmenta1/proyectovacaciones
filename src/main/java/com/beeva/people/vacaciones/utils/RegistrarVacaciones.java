package com.beeva.people.vacaciones.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.beeva.people.vacaciones.dto.VacacionesDTO;
import com.beeva.people.vacaciones.empleado.pojo.Empleado;
import com.beeva.people.vacaciones.empleado.pojo.RegistroVacaciones;

public class RegistrarVacaciones {


	public synchronized int DiasTranscurridos(Date fechaInicial, Date fechaFinal) {

		DateFormat df = DateFormat.getDateInstance(DateFormat.MEDIUM);
	    String fechaInicioString = df.format(fechaInicial);
	        try {
	            fechaInicial = df.parse(fechaInicioString);
	        } catch (ParseException ex) {
	        }

	        String fechaFinalString = df.format(fechaFinal);
	        try {
	            fechaFinal = df.parse(fechaFinalString);
	        } catch (ParseException ex) {
	        }

	        long fechaInicialMs = fechaInicial.getTime();
	        long fechaFinalMs = fechaFinal.getTime();
	        long diferencia = fechaFinalMs - fechaInicialMs;
	        double dias = Math.floor(diferencia / (1000 * 60 * 60 * 24));
	        return ((int) dias);
} 
	
	public int DiasPermitidos(int diasTrabajados) {
		int diasVacacionales=0, anios=0;
		
		if(diasTrabajados >= 365 && diasTrabajados < 730) {
			diasVacacionales=6;
		}
		else if(diasTrabajados >= 730 && diasTrabajados < 1460) {
			diasTrabajados -= 365;
			anios = diasTrabajados / 365;
			diasVacacionales = 6 + ( anios * 2);
			}
		else if(diasTrabajados >= 1825) {
			diasTrabajados -= 1825;
			anios = diasTrabajados / 1825;
			diasVacacionales = 14 + ( anios * 2);
		}
		return diasVacacionales;
	}

	public List<RegistroVacaciones> RegistroArrayVacaciones(VacacionesDTO vacacionesDTO){
		List<RegistroVacaciones> listaRegistro= new ArrayList<>();
		
		for(Date fecha : vacacionesDTO.getFecha()) {
			RegistroVacaciones registroVacaciones = new RegistroVacaciones();
			
			registroVacaciones.setDiasTomados(vacacionesDTO.getDiasTomados());
			registroVacaciones.setIdEmpleado(vacacionesDTO.getIdEmpleado());
			registroVacaciones.setFecha(fecha);
			
			listaRegistro.add(registroVacaciones);
		}
		
	return listaRegistro;
		
	}
	
	public int diasTomados(List<RegistroVacaciones> listRegistroVacaciones) {
		int diasTomados=0;
		
		for(RegistroVacaciones contadorDias : listRegistroVacaciones) {
			diasTomados+= contadorDias.getDiasTomados();
		}
		return diasTomados;
	}
	
	public int diasTrabajados(RegistrarVacaciones registrarVacaciones,RegistroVacaciones registroVacaciones, Empleado empleado) {
		int diasTrabajados=0;
		diasTrabajados= registrarVacaciones.DiasTranscurridos(empleado.getFechaIngreso(),registroVacaciones.getFecha());
		
		return diasTrabajados;
	}
	public int diasPermitidos(RegistrarVacaciones registrarVacaciones, int diasTrabajados) {
		int diasPermitidos=0;
		diasPermitidos=registrarVacaciones.DiasPermitidos(diasTrabajados);
		return diasPermitidos;
	}
	
	public int diasRestantes(int diasPermitidos, int diasTomados) {
		int diasRestantes=0;
		diasRestantes=diasPermitidos - diasTomados;
		return diasRestantes;
	}

}
